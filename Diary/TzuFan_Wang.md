# 2021-09-23 #

### Feedback about Conflict View
* In my opinion, I am sometimes in harmony view, and sometimes in conflict view. 
* It all depends on the confidence of how much did I know about the issue. 
* If I can handle the situation, I will be a positive person when facing conflicts. 
* Something like academic issues or something that I can make my own decisions, I will be fine. 
* However, there are still some problems that destroyed my mind. 
* For example, my girl friend got epilepsy last two weeks. That was the most serious conflict I’ve never met before. 
* When something out of control, It seems that I would change to a person in harmony view.

### Difference between Group and Team
* Group: Only be grouped together, but don’t know each other.
* Team: Work together smoothly and achieve same goals.

# 2021-09-30 #

### Feedback about "We are in The Era of Code"
* Professor told us that in nowadays world, we are strongly connected with Codes.
* In my opinion, I thought that this comment is correct.
* Not only in the domain of Computer Science, but also in the domain of Mechanical Engineering,we all need to learn the computer language! 
* this will be the world,s trend.

### Feedback of Steven Pinker's Claim
* I would like to share my own opinion about his claim.
* Obviously, in his presentation, he had told us that we are in the era that progresses in a good way now.
* However, there are still lots of issues that he hadn't discuss in his report.
* He had just talked about changes in "Human", but there a large amount of species that he hadn't discussed.
* On the other side of those pros to human, animals and plants are suffered from dangerous situations.
* We should make the conclusion in more cautious way.

# 2021-10-07 #

### Feedback about the Questionnaire
* I think that I am not actually good at "financial knowledge".
* In my opinion, money plays a significant role in the world.
* However, we actually touch this area only when we get our first job.
* Since that we don't have enough money to invest without salaries.
* We can only talk about investment, but cannot really earn a lot on it.

### Feedback about the Class
* Today I learn a lot of "Money".
* I think the most interesting part is the part discussing "What is money?".
* Money is fungible, durable, portable, recognizable and stable.
* I didn't really search the meaning of money before.
* Actually, it is quite interesting!

# 2021-10-14 #

### Feedback about the TED talk: Why fascism is so tempting
* The speaker mentioned the problems that fascism may cause, 
* and how such a new dictatorship will control our thoughts through technology and machine learning. 
* This sounds creepy and thought-provoking. 
* In my opinion, Fascism and Democracy, and the discussion of this topic seems to be far from my daily life. 
* In fact, this problem exists in our daily lives. 
* Under the premise that we strive to pursue rapid integration and analysis of data. 
* Correspondingly, will other social issues arise? 
* I think this is a problem that our generation needs to face.

### Feedback about the TED talk: My descent into America’s neo-Nazi movement

* When I heard of the speaker saying that 
* "You see, it's our disconnection from each other. Hatred is born of ignorance. Fear is its father, and isolation is its mother."
* Actually, I cried out in a silence.
* When we don't understand something, we tend to be afraid of it.
* If we keep ourselves from it, that fear grows, and sometimes, it turns into hatred.
* It was almost talking about my childhood.
* My father and mother was always working when I was a child.
* At that time, I hate the whole world.
* Hate those bulliers bullying me because of my fat body.
* However, I hadn't thought before that the problem is my bad personality that time.
* When I finally realized this, it was time for me to graduate from high school. 
* But time is gone, I can only tell myself now and in the future.
* "To understand each other more, so as not to cause hatred that shouldn't exist."

### Feedback about "Yuval Noah Harari on Imagined Realities"
* Fictional reality creates objective reality.
* In fact Lawmakers are lawbreakers.

