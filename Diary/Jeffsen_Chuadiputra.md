# 2019-09-19

* The lecture is great and understandable.
* There is an explanation on how the direction that the world is going to progress in the next decade.
* The material that is given aka the video that is shown has shown me that in actuality, it's not the world that has gone bad nor that the world has gone stagnant. In fact the world is progressing toward a better future, but the over exaggeration of the media that misled people to think that the world right now isn't better than the last decades.
* The future of economics, in cars, electronics and one of the most valuable resources right now, oil.

# 2019-09-26

* Today we learned about fake news and how to discern them
* News is a powerful weapon that can be used to aid or against people in the world
* Fake news is a very dangerous material that can spread misunderstanding and create conflict

# 2019-10-03

* Today we learned about how economy works and how it affects people
* People felt the effect of money when there is a tangible item that can be held or seen
* Economy is a big cycle which rises and fall
* Great economy creates more spending, loaning and borrowing money which in turn create a better economy
* Good economy makes people borrow money until there is no money left and debt so high no one is able to pay back which in turn crashes the economy.

# 2019-10-17

* Nationalist and fascist are not the same
* With nationalist you held proud to those identities that made you
* With fascist you look down on others who is different from you
* Hate doesn't come naturally, usually hate group is juat a group of people who is unsure of themself, their identity and where they belong
* Taken advantage by bad influence made them hate everything in hte world
* To be able control people is a powerful weapon
* Democracy which many nations are built from is not a way by logic, rather by feelings of its people

# 2019-10-31

* I didn't attend class, so everything i had written is based on the video i watched.
* Depression is something very serious
* The way to help depression is understanding the depression it self
* It's okay to be sad and okay
* Depression is not a weakness nor it's abnormal. But depression is something we must solve together.
* The reason to suicide is helplessness, thinking the world won't become better.
* If we know someone that has a suicidal thought, we must confront them.
* Most people who survive from suicide acts regrets doing suicide in the first place.

# 2019-11-07

* How to forgive
* No matter what have been done we must forgive for the betterment of our life
* How to live more healthy

# 2019-11-14

* We have a discussion about changes we wanted in our lifes, whether it's internally or externally
* The main topic of the lecture is the legal system ( UK ).
* How UK citizens should know the law and how to stand up for their own rights
* Nobody has soverignty over another

# 2019-11-21

* The first video shown how the internet was at the beginning and how people of the past's expectation for the internet
* How the vision of internet for the future turned into a big mistake
* The big companies has turned the social media into brainwashing device
* Millenials have problem but it wasn't their fault
* Millenials problem stems from bad parenting
* They don't have coping mechanism because the lack of social connection
* phones become an addiction

# 2019-11-28

* How the media works and how it spread information to us
* How media and news sites have their own bias and agendas
* Do research before believing

# 2019-12-05

* The world has it's own factors that is divided into 9 planetary boundaries
* To exceed the usage of the boundaries means doom for humanity

# 2019-12-12

* Spent most of the class debating
* Questions and answers for each planetary boundaries

# 2019-12-19

* Debate regarding the questions given prior
* Divided into three groups : Capitalist, workers and naturalist

# 2019-12-26

* read diary
* Tell success rules

* Thursday :Unproductive, successful, finally had a good rest after lack of sleep because of homeworks.
* friday : Productive, unsuccessful, Do group task for hours but didn't finish
* saturday : Productive, unsuccessful, still doing the same task and finishes halfway
* Sunday : Productive and successful, Finishes the task, finishes editing the video for the task
* Monday : Unproductive and successful, present the video although the presentation is quite easy
* Tuesday : unproductive,unsuccessful day before new year, didn't study just play around
* wednesday : unproductive, unsuccesful, holiday, so done the same thing

