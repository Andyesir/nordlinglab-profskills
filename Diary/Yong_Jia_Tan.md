This diary file is written by Yong Jia Tan 陳勇嘉 E14085066 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2021-09-23 #

* The second lecture starts with an introduction on conflicts. It allows me to have a better understanding about how conflicts can be organized into different levels.
* Learned about GITHub and how it could be used as a platform to discuss and coordinate work.
* Learned about **Markdown**, which is commonly seen on Whatsapp for my case. Useful as an emphasis on certain terms.
* Acknowledged that there are a lot of objects in our surroundings that follow the rule of exponential growth leads to a world of abundance.

# 2021-09-30 #

* The third lecture placed an emphasis on statistics, which is a useful tool to allow people to know about trends and the world around them.
  - Main problem with statistics is that it could be easily manipulated by only obtaining desirable data or skewing the results of the data to suit certain contradictory conclusions.
  - Everyone should have the ability to differeciate between good data and bad data, not just believing every data that you see. Try to do your own research.
* Fake news is all over the place due to people who are lazy to fact-check the information that they received.
  - The same thing applies to statistics, be vary of any information presented and try to do your own research.
  
# 2021-10-07 #

* The fourth lecture gave a brief introduction on the Financial System, which is mostly based on credit after the abolishment of the Gold Standard by President Franklin D. Roosevelt.
* The current state of the global Financial System is heavily skewed towards the rich, benefiting them as the more money you have, the greater the power to amass more money.
* From the recent 2008 Financial Crisis, we can know the effects of an unregulated banking industry where credit has been used as a tool to make profit, but ended up hurting themselves instead due to mismanagement.
* I found it absurd about the fact that the professor brought up, which is millions of NTD could just appear from nothing by just taking a loan, which shows that this system has a terrible flaw, since it is not regulated by anything solid other than credit.

# 2021-10-14 #
* Populism is on the rise everywhere, especially in the Western world more recently, as people are getting more negative views on the so called "Threats" that would affect their wellbeing.
* Countries like Poland, Germany and Hungary has a sizeable following of populism and nationalistic societies.
* When you don't really know each other, you will be prone to misinformation being spread by intentional people, which is concerning since this would only reinforce their false beliefs onto certain groups.
* Marginalised groups are the groups that should get more care, but with misinformation being spread around they would only recieve hate.
* Malaysia, the country that I'm from, suffers from people not trusting each other, which has hindered it's development of being a power in South East Asia despite having the resources to become one.
* People should try and learn more about each other, so that we can live in a more compassionate world.