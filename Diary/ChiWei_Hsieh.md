This diary file is written by Chi Wei, Hsieh in the course Professional skills for engineering the third industrial revolution.
Chinese name: 謝濟緯
Student ID: I54091149

# 2021-09-23 #

* Still trying to fugure out what git is about
* Since electric cars are gonna be a lot cheaper, I'm gonna start investing in Tesla right now

# 2021-09-30 #

* The ability to identify fake news is important
* Never blindly believe nor reject any given information
* The method used to require statistical data has a great impact on the validity and credibility of the results 
* Sources are important as they provide authenticity to data

# 2021-10-07 #

* Loans create money, that's just so cool, I wanna make money too
* Income inequality is terrible, the rich should help the poor more

# 2021-10-14 #

* People who lack the emotional support that they need might go on dark paths
* Ignorance leads to most conflicts
* We should know ourselves better, so that we don't get fooled by extremist ideals