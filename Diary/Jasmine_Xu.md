This diary file is written by Jasmine Xu F34096079 in the course Professional skills for engineering the third industrial revolution.

2021-09-23
Learn about GIT and how to use it
Introduction of the professor and his personal and educational background
An insight on conflict escalation chart and its outcome

2021-09-30
Learn how to cite for our presentation (journals, website, etc)
Detecting fake news

# 2021-10-07

* What gives money its value.
* Learn about how our financial system works
* Money is created by the bank when we are taking loans
* Bank has the power to create and print more money
* Extreme inflation can happen when more money is created if there is a low demand for the currency

# 2021-10-14
