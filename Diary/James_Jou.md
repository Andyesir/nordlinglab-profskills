This diary file is written by james jou E34085036 for the course ~ Professional Skills for Engineering: The Third Industrial Revolution.

2021-09-25

* Learnt about GIT and how to use it
* Introduced to the professor and his background (both personal and educational)
* Got an insight on conflict escalation chart and its outcome



2021-09-30

* Learnt on how to cite for our presentation (journals, website, etc)
* Detecting fake news

2021-10-07

* Learnt on what gives money its value. 
* Learnt about how our financial system works
* Money is created by the bank when we are taking loans
* Bank has the power to create and print more money, however, extreme inflation can happen more money is created if there is a low demand for the currency