DAIRY BY SHAO KAI HUANG
STUDENT ID: E14056384

# 180913 #
* I was not in the class yet.
	
# 180920 #
* The first class for me. I still not realized how the professor would teach us and also not knew about what the professor would teach us in this class.
* The first test came out, but it didn't count. 
* This testing style really shocked me when i knew that we'll have an exectly same exam in the end of this semester. 
* That is to say, we have whole semester to find the answers for those questions we had saw, very different from other classes i had done in NCKU. 
* Every student had thier presentation about what they learned last week. It's a good chance to proof what we just had learned is correct or not and also to practice our English ability. 
* Ted Talk Show is also included in this class. 

# 180927 #
* My first presentation.
* Nervous, easy to forget what i planned to say.
* My fisrt time to use Bitbucket.
* Three Ted Talk tell us to have the ability to identify the truth and fake.

# 181004 #
* Diary structure is important to make it easier to read.
* Average doesn't show a lot of messages.
* Money is important. We need money to build things, so, we have to understand how financial system works.
* The first speaker, the proffesor, speaks too fast that I can just understand about 30%. I have to improve my English ability.

# 181011 #
* The claim must be affirmative sentence with details, and proof if it's true or not.
* Most of logos are belongs to copy right.
* Mutiple effect, ripple
* Leverage, we can buy something by using not that much money because personal credit.
* Money will lost most of its value in the long term, when people don't trust it anymore.

# 181018 #
* Sorry, I was not in the class today because of the competition.

# 181025 #
* 350,000,000 peoples are depressed.
* Students get A are easier to depress.
* Depression feels entity.
* Depressed friends might be free labors.

# 181101 #
* We often belittle others without notice. Ex: Using cell phone while talking.
* Desperation is caused by lacking something meanful in our life.
* Experiment shows that we often explain the choice after work.
* About recent Ted Talks we have seen, I think those who have a speech by telling their own experience are more difficult for us to have clear claim.
* In my opion, i suggest to cut down the time on presentation and have more time to discuss or debate after the Ted Talks. 
* It doesn't mean that presentation is not important, but time is limited.

# 181108 #
* Innocent until prove guilty.
* Common law's meaning.
* We need to know more about the law to protect our own rights.
* During the vedio, I considerly felt the weakness about my English ability without the caption. Reviewing the vedio is necessarily needed in this situation.

# 181115 #
* Every thing on the Internet should be free because if a person is too poor to afford it, it will be very unfair.
* Social media becomes a behavior modification empires.
* Being like or unlike becomes a worry for most of people.
* Negative stimulus feedback speed is greater than positive stimulus feedback speed.
* We are in the period calls "Pick TV".
* Google and customers are tripped in the same economic loop.
* We are highly addictive to Technology (social media). It becomes imbalance and destorys relationship.
* A lot of things and concept that told by the second speaker are really really sensible. Those problems truely show in our daily life. However, we still can't find the balance easily, even we know the problems.

# 181122 #
* This time of vedio is talking about history. It's a little bit too hard for me.
* The presentation that professor giving us this time may be a little complicate.

# 181129 #
* Everyone have their own opion to comprehend the persentation's requirement.
* This time of presentation still took too much time to complete.
* Expect the debate next class, we never try before.
* Before the vedio watching, I had knew that our Mother Earth was in a serious situation. However, after the vedio watching, I finally knew how bad was the situation.
* A lot things have to be number quantized.
* The concept of nine planetary boundaries is new for me.

# 181206 #
* The planet boundary problems really impressed me a lot. Before the presentations, i never know what exactly the nine boundaries were.
* New mode of the discussion after the presentation, more interesting than only Q&A.
* During the debate, i thought more about how to make others understand the idea i want to express than only how to the speech well.
* There's more interaction by doing the debat, and i think it's a good way for me to not fall asleep, if the presentation is really boring.
* The disadvantage is that there's a lonely poor guy.

# 181213 #
* The debate this time is also quite interesting, but the content is too much for us to complete in one class. 
* The things make me feel successful and productive:
	1. Good arrangement of schedule
	2. Focus on one thing only
	3. Good control system for everything, including where the money goes
	4. Wake up at the right time
	5. Exercise regularly
	6. Do the right thing and do the thing right
* The rules:
	1. Always take a bath before the sleep.
	2. Sleep at least 6 hours.
	3. Do everything to a paragraph without any interrupt.
	4. Question everything i am not sure, trust myself.
	5. Arrange things to do and make them on the right position.
# 181220 #
* For lecture:
	* Reducing the necessary use, such as land and water, to grun the agriculture is amazing.
	* It makes me feel like we are limited in the current thinking mode, that keeps us away from advance.
	* Some different direction of solving a problem may sound impossible, but actually it isn't, only the obtacle we have to cross over.
	* I know happiness is more important than other things, but not easy to recieve. Work force me to be rseponsible.
	* Budan teaches me to use the resource very carefully, not only the nature source but everything i can controll.
* For daily record about productivity:
	* 181220 (unproductive)
		1. Because of the class at eight o'clock, I have to wake up early.
		2. 7 classes a day, i thing i am hard-working enough.
		3. Studying for Probability and Statistic after the class until 7 o'clock, and went for dinner.
		4. After reaching dormitory, my teammates of control system cameto me and mindstorm for the term project, but end up with the mystery.
		Conclusion : 
			Even feels likes working all the day, the things had been done seem like not that many; so, it make me feel unproductive. 
			I'll try to be more focus on only one or two things a day, and finish it at once, instead of several days. Also, exercise will be necessary.
	* 181221 (unproductive)
		1. Wake up at 8 o'clock and go to the gym.
		2. After the breakfast, go to study for Probability and Statistic that i didn't finish yesterday and go the class at 1 p.m.
		3. Discuss about the Ev3 project with TA, and get a maybe solution to fix our current problem.
		4. Research the braking system design about FSAE and go to the schedule meeting at 9 and a half.
		5. Ran home for discussion about volunteer progrem during the winter vacation after the meeting.
		Conclusion :
			Too many things in a same day, i prefer to do less thing but longer period. Maybe try to focus on only one main subject tomorrow. 
	* 181222 (productive)
		1. Wake up at 7:30 a.m. and play the badminton until 12 o'clock.
		2. Discuss with FSAE team until 6 p.m.
		3. Research for braking system and find the products we need until midnight.
		Conclusion :
			Focus on only two major things and made the process to a paragraph. Comfortable, but Mechine Component Design is waiting for me.
	* 181223 (unproductive)
		1. Wake up very late at 11 o'clock and go to study Mechine Component Design til i lie on the bed.
		2. During 7 to 10, Ev3 mindstorm.
		Conclusion :
			Nothing but unproductive, low consentration.
	* 181224 (unproductive)
		1. a lot of classes
		2. go to figure store for the special Xmas event
		3. discusion with STUT student about braking system of FSAE
		Conclusion :
			Actually, i learned a lot during discussion, but the school's work wasn't done yet.
	* 181225 (productive)
		1. Merry Chistmas
		2. No gift and got a bill
		3. Figured out the matlab code of Probability and Statistic
		4. Ev3 until midnight at school
		5. Holly
		Conclusion :
			Ev3 schedule made a big leap. Matlab solution in the pocket. Not bad.
	* 181226 (unproductive)
		1. Matlab code writing
		2. Ev3 heartbreaking
		Conclusion :
			Ev3 is shit.
		
# 181227 #
* Thanks a lot of the speakers for telling us what is the blockchain.
* It really impressed me about the concept of blockchain, i think if it can come true, the world will create another new type of econamic mode.
* I am looking foward to the two speakers's success.

# 190103 #
* Successful and productive i feel everyday
	* 190104 Fri
		A. Successful and unproductive
		B. I woke up too late and the first thing was eating lunch, but i was focus on the class afternoon and exactly know the content the professor taught.
		C. I would like to wake up more earlier to get more time in a day.
	* 190105 Sat
		A. Successful and productive
		B. I woke up at 5 am because of the high-speed railway to Taipei. Finish most of study of Electronic and join the rehearsal of Nepal volunteer.
		C. Sleep earlier and wake up earlier tomorrow because of the less sleep today.
	* 190106 Sun
		A. Unsuccessful and unproductive
		B. Treval from Taipei back to Tainan, too tired to study more. I slept earlier but not that early.
		C. Study at the deparment, be more concentrate.
	* 190107 Mon
		A. Successful and productive
		B. Finish the exam of Mechanical component design, feel good no matter how's the score. Finish the HW of Probability and Statistic.
		C. Focus on study for Control Systems.
	* 190108 Tue
		A. Successful and productive
		B. Got the expact score on the EV3 presantation. Also, shot the vedio we need for the presantation and studied Control system.
		C. Finish whole study of the exams on Thursday.
	* 190109 Wed
		A. Unsuccessful and productive
		B. Learning Nyquist. Learn a lot but not enough.
		C. Not to be a dead man tommorow.
	* 190110 Thu
		A. Unsuccessful and productive
		B. A lot of exam have been done today, but control system is shit.
		C. Be safe and sound.