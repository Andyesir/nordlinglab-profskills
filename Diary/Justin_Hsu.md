This diary file is written by Justin Hsu E14051334

# 180927
 * Learned lots of presentation skills and slide making tips.
 * Been frustrated by my awful presentation.
 * Known a useful tool: Gapminder.
 * Known a cool website: Stopfake.
 * Leraned how to identify fake news and the way to verifying it's ture or not.
 * Lerned the data may have error depends on the range we choose.

# 181004
 * Learned the differences from arithmetic mean, geometric mean and median in Baza's presentation.
 * Learned that the economy is based on the cycle about slary, credit and debt.
 * Knowing that what bank did is not actually we seen.
 * Theworld economy will be going down in nearly future.
 
# 181011 
 * The international date format is "year-month-day".
 * The copyright of the logo is usually owned by the organization.
 * What the banks lend out is not the people's deposit but more like numbers based on credit.
 * The value of money is originate from credit.
 * Bank will bankrupt if people all pick up their money at the same time.
 * Fascism roughly means the absolute loyalty to the country.
 * As technology advances, people may be easily control by others since program knows people than themselves do.
 * Ignorance leads to fear and fear leads to hatred.
 
# 181018
 * Knowledge is something that people believed and has been heavily verified.
 * Who has knowledge is responsible to share and transfer it.
 * Exercise has good influences to our brain.
 * Morden "health care" systems tend to be "sick care" system, which means that the systems tend to make people easier to treat their sick,
   not to keep healthy. 
   
# 181025
 * Melancholia patient often behave even happier than who don't has melancholia.
 * We should act as usuall when we get along with melancholia patient.
 * People who want to commit suicide need to be listened.
 * Every one all has chance to get melancholia.

# 181101
 * What we know about ourselves might not be so accurate.
 * We tend to give good reasons to things we thought "right".
 * Don't pursue happiness, instead, find the belong, purpose, things detached, and stories of your life.
 
# 181108
 * This lesson made me to doubt the public authority.
 * If you didn't do any thing illegal, you don't need to do any thing that police required.
 * Innocent until the guilt proven.
 * Signature is powerful these day.
 
# 181115
 * There's dark side along with the evolution of technology.
 * the new generation are tend to be lack of patient since the technology nowaday make the world faster and faster.
 * People nowaday are addicted to the internet, aspecially social media, and often ignore the relationship in real life.
 * Algorithm is usually recognized as complex, unreachable mathematical. 
 
# 181129
 * Knowing the nine planetary boundaries.
 * Growth is not always a good thing.
 * Our economy should be more environment-friendly.
 
# 181205
 * Communication, transportation and energy technology are the three things of industrial revolution.
 * Sharing economy is the future trend, e.g. Youtube, Napster, Wikipedia.
 * We all need the new consciousness for a new era.
 
## 181213
 * I feel successful and productive today because I just finished an exam and my report, and act well on leading the student association of ME.
 * I will do things more initiative tomorrow like keep studying.
## 181220
 * Five rules to success:
  1. Figure out what I want to do and what I need to do.
  2. "I can do it."
  3. Do it.
  4. Learn from the experience.
  5. Never regret.
  
## 181220 Unsuccessful Unproductive 
 * Unsuccessful because I failed a quiz in the morning.
 * Unproductive because I copy the homework and the report which shuold be handed in tomorrow.
 * Try to be more resposibility to my academic.
## 181221 Unsuccessful Productive
 * Unsuccessful because I still don't study.
 * Productive because I finished a team report and done many student association bussinesses.
 * Still need to try to be more resposibility to my academic.
## 181222 Successful Productive
* Successful because I started studying.
* Productive because I done many student association bussinesses and finished a chapter of mechanics of materials.
* Be more focus on what I'm doing.
## 181223 Successful Productive
 * Successful because I studied all day and I have reached my goal.
 * Productive because the same reason.
 * Keep the good feeling.
## 181224 Unsuccessful Unproductive
 * Unsuccessful because didn't reach my study goal.
 * Unproductive because the same reason.
 * Try to relax myself when the pressure is too heavy.
## 181225 Unsuccessful Unproductive
 * Both the same reason of yesterday.
 * Never give up.
## 181226 Unsuccessful Productive
 * Unsuccessful because I failed two exam.
 * Productive because I finished an exam and a report.
 * Don't let bad mood influence my behavior.
 
 
## 190103 Successful Productive
 * Successful because having good progress on team project.
 * Productive because studied a lot in night with friends.
 * Take a little rest since just finished an exam, but keep studying.
## 190104 Successful Unproductive
 * Successful because just finished an exam and took some rest.
 * Unproductive because didn't study all day.
 * Start study tommorrow.
## 190105 Successful Unproductive
 * Successful because having a dinner with the members of student association.
 * Unproductive because studied little.
 * Start study tommorrow.
## 190106 Successful Productive
 * Successful because studied all day and well.
 * Productive because the same reason.
 * Be careful and focus when take the exam.
## 190107 Successful Unproductive
 * Successful because our LEGO EV3 project going well.
 * Unproductive beacuse spending lots of time on the project, and I'm actually helpless.
 * Use the limited and trivial time to study.
## 190108 Successful Unproductive
 * Successful because our LEGO EV3 profect got a high score.
 * Unproductive because spending lots of time on the report of fluid mechanics and studied little.
 * Keep study and don't use cellphone during rest.
## 190109 Unsuccessful Productive
 * Unsuccessful because study makes me tired.
 * Productive because studied all day long.
 * Be careful and focus when take the exam.


