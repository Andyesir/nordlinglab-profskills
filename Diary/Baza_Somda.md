 This is a diary by Baza Rodrigue SOMDA F04057150
 
## 180920
 
 * I just enrolled in this class so this is my first lecture.
 * Steven Pinker's talk was eye-opening.
 * I didn't realize the extent of the progress made by humanity in many areas especially peace.
 * The negative media reports are certainly the reason why most people have a gloomy view of our world.
 * Isn't this negative noise around us a reason why many today suffer of depression and hopelessness?
 
## 180927
 
 * It is essential to learn how to cite properly our sources to avoid plagiarism issues and lawsuits.
 * Today more and more people don't trust government statistics; they think those statistics may be rigged.
 * It is important to collect data on the problems of our societies if we want to actually fix them.
 * People should be trained in detecfting bad or misleading statistics.
 * Beware of too much certainty in the data. This might hide bias from the organisation realising the study.
 * Averages can be misleading in non-symmetric samples; we need to make sure to detect and isolate outliers.
 * Good data has to reflect society and be easily readable.
 * "When you change the scale you can change the story"
 * Government statistics are usually better than private ones because they seldom reflect personal interests.
 * Hans Rosling's talk was quite sad for someone like me coming from Africa. 
 * It's true that high wealth exists in Africa but it is mostly the continent of poverty by today's standards.
 * What are the true, not so obvious, reasons why we trail behind?
 
## Swedish Article
 
 * The article is about the effect of civil nuclear plants on the climate.
 * The original study was based on old technology (gas diffusion).
 * It estimates the time to build a nuclear power plant to 19 years while it's actually about 5.5 years.
 * It claims that civil nuclear power participates in nuclear proliferation.

 
## 181004
 
 * We are not taught about the financial system at school and that is a major shortcoming.
 * I need to get more information about it in order to know how to take advantage of it.
 * Do banks really create the money supply?
 * When banks are really big and concentrated it leads a disconnect between the financial sector and the actual value created on the market
 * What is the difference between a hedge fund and a holding.
 
## 181011
 
 * The value of currency is based on people's trust in its inherent value.
 * A way to prevent crisises might be to make sure banks don't become to big to fail.
 * The Marginal propensity to consume shows how a single dollar spent can result in even more spending by the multiplier effect.
 * Does bitcoin really spend more of Earth's ressources than paper currency?
 * " Countries without strong nationalism tend to be violent and cruel " Yuval Hariri.
 * " Good people who remained silent are sctually the reason why the 2nd WW was started " Prof Nordling. 

## 181018

 * Plato defines knowledge as a "justified true belief".
 * A single cannot be effectively used to prove a claim; though a single one can be used to weaken one.
 * Careful, don't confuse nationalism and fascism.
 * Exercise improves long-term memory and attention span.
 * There are immediate effects as well as long lasting effects of exercise on the brain.
 * Exercise causes the hippocampus to create brand new brain cells.
 * Most countries' health-care system are actually "sick-care system".
 
## 181025
 
 * 7% of Americans experience depression.
 * To help someone suffering with depression, words are usually not the most important thing.
 * "A-student are more likely to have a bipolar condition".
 * Functional depression is caaracterized by the inability to "just get over it".
 * Don't be too fixated on trying to fix people with depression.
 * Some signs of deep depression and suicidal risk: hopelessness, helplessness, social withdrawing...
 * "Depression is the leading cause of disability in the world".
 * " Suicide rate among black kid has doubled in the last 20 years".
 * "70% of the people that seek treatment get better".
 
## 181101

 * Perhaps we are continually making things up.
 * If you make people come in  contact with the opposite view, it might make them change their own mind.
 * "Most of what we think is knowledge is actually self-interpretations".
 * Keep in mind that you might not know yourself as well as you think.
 * "Violence is a learned behavior".
 * Meditation seeems to have great benefits.
 * "Sustained good-will creates friendship..."
 * A lack of meaning creates emptiness.
 * People who chase happiness tend to be quite unhappy.
 * People who have a meaning in life tend to be more resilient and live longer.
 
## 181108
 
 * "A birth certificate is an ownership certificate for the state".
 * Is Legalese really design to fool the common people?
 * Can taxation be compared to extortion ? 
 * An IOU is a promisory note; its says you acknowledge owing money to someone.
 * As a "sovereign citizen, if you didn't commit a crime, you have to give a consent to be searched an can refuse to state your name.
 * Common Law is the law of the Land (lawful) and is different from acts of congress/parliament (legal).
 * What are the caracteristic of a crime under  common law?
 * Do policemen actually follow their oath?
 
## 181115

 * The "free-to-use" technology companies that base their income on ads are becoming behavior modification companies.
 * People with good intentions created companies that are basically evil.
 * "sometimes when you pay for stuff, things get better"... Mayber the way out of ad-culture is to start paying to use social app.
 * Or at least create an option for those who want to pay extra dollars to avoid seeing ads and having their data sold to companies.
 * social apps stimulate dopamine release just the same way alcohol and smoking does,leading people to dependancy.
 * It takes a long time to build a skill and our instant-gratification generation is finding it harder to actually master anything.
 * Don't believe algorithms are free of bias and prejudice just because they are based on computer technology.
 * Algorithms can be "opinions embbeded in code" and even good intended ones can lead to wrong decisions.
 
## 181122

 * If we look carefully at the past, we can find cycles and by studying those cycles we might be able to "predict" the future to some extent.
 * Forgetting the past is condamning ourselves to repeat it again and again.
 * The Roman Republic has a lot of common traits with the modern USA (especially politically and financially).
 * we must be careful to check all the information we get, and avoid flooding ourselves with fake news.
 
## 181129

 * Our modern civilization is addicted to growth.
 * How can global economy keep growing exponentially while our ressources are finite?
 * Dematerialized economy doesn't really help because structures like the internet or the cloud still require material infrastructure and energy consumption.
 * Fast and unbounded growth isn't necessarilly good: cancerous cells are in fast and unbouded growth too.
 * Boundaries for our ressources might actually be a good thing because it will force us to think creativily and unleash our full potential.
 * Have we entered the antropocene?
 * The decade is over but sadly we haven't managed to bend the curves.
 * The nine boundaries are an ongoing challenge.
 
## 181206
 
 * The planetary boundary for freshwater use might already be exceeded.
 * When I think about all the changes human behavior is making to the nature, I wonder if can really do anything to invert them.
 * The 2nd industrial revolution was fueled with oil and gas, bringing a terrible burden on our planet.
 * A 3rd industrial revolution seems needed if we want to stay alive for much longer.
 
## 181213

 * Our capitalist societal structure imprisons the worker in an endless chase for financial stability through alienating work.
 * Maybe delinking revenue from work is a good idea if we want people to actually thrive in what they love and bring unquantifiable value to Humanity.
 * Automation can be a good way to achieve this if the profits are shared more equally through systems such as a basic income.
 * It might be hard to make greedy capitalists understand that such a system can be good to all, even them (people will riot if they can't have enough to live decently).
 * Having a stable income might actually mean being relatively poor cause the salary revenues are much much lower than capital revenues.
 
## 181220

 * The present model of intensive agriculture is no longer environmentally sustainable; Alternatives such as hydroponic agriculture are welcome
 * I think replacing politicians by random people will bring unforeseeable problems that might be worse than the democratic system.
 * Working on fixing democracy by instituting popular initiative referendums can be the beginning of a solution
 * Appointing a commitee a randomly selected people to regularly control government and legislative action is another solution.
 
## 181227

 * Very interesting presentation by the people from UnityChain. They seem to know their topic really well.
 * A completely decentralized world could be a good way to reduce financial inequalities buy removing the gatekeepers that can be corrupted.
 * I only hope we can solve the energy problems related to blockchain technology.
 

## 190103

 * Learning how to gain people's trust is a skill that can help us in all our future endeavors.
 * Looking for a partner in life is hard and can result in heartbreak, but that does'nt mean you should give up.
 * Maths can give us an insight into what makes a relationship fly or fail.
 
## Daily journal

 * 190103 Thu
 
           * A. Unsuccessful and Unprodructive
           * B. I went late to all my classes and didn't open a book or do anything remotely useful.
           * C. I will leave earlier for all my classes and try to make at least 30 mn of productive work per day.
  
 * 190104 Fri 
 
           * A. Successful but unproductive
           * B. We did our final capstone project presentation but I didn't study that day
           * C. I will keep nice souvenirs of my capstone, I need to study tomorrow
           
 * 190105 Sat
 
           * A. Successful and productive
           * B. I went to the library and studied a lot and I called old friends back home to catch up.
           * C. It feels good to be finally productive again, I will go to the library tomorow.
      
 * 190106 Sun
 
           * A. Unsuccessful and unproductive
           * B. I slept until 4pm and didn't have the willpower to open a book.
           * C. I will pray for tomorrow's exam to be good
          
 * 190107 Mon
 
           * A. Sucessful and productive
           * B. Today's exam went really well, after it I managed to finish and hand in my Automatic control final project
           * C. I will study tomorrow morning and hope for the day to be similar to today.
       
 * 190108 Tue
 
           * A. Unsuccessful but productive
           * B. I did extremely bad at automatic control final exam but I managed to prepare tomorrow's combustion exam 
           * C. Next semester I will make sure I don't get surprised but final exams.
           
 * 190109 Wed
 
           * A. Successful and productive
           * B. Good exam today and I finished writing my last report.
           * C. Today was my model day, I hope it can always be like this
           
 
 
## 190110

 * Our world today is a mass surveillance society
 * This is sad because not only it's an infringement of our freedom and privacy, but it's also not useful. Too much data can make good analysis impossible.
 * What China is about to become is even scarier than what i could have imagined
